describe("Multiplication", () => {
    dataTest = [
        {
            array: [1, '*', 1, '='],
            expected: '1'
        },
        {
            array: ['-', 1, '*', 1, '+'],
            expected: '-1'
        },
        {
            array: [1, '*', 0, '.', 2, 5, '='],
            expected: '0.25'
        },
        {
            array: [1, '*', 0, 0, 0, 0, '.', '.', 2, 5, '='],
            expected: '0.25'
        },
        {
            array: ['*', 5, '='],
            expected: '0'
        }
    ];
    dataTest.forEach(data => {
        const {array, expected} = data;

        for (let i = 0; i < array.length; i++) {
            document.getElementsByName(array[i])[0].click();
        }

        const actual = document.getElementById('inputField').value;

        it(`Should return ${expected} when input data was ${array} `, () => {
            assert.strictEqual(actual, expected)
        });
        Clear();
    })
});


describe("Division", () => {
    dataTest = [
        {
            array: [1, '/', 1, '='],
            expected: '1'
        },
        {
            array: ['-', 1, '/', 1, '='],
            expected: '-1'
        },
        {
            array: [2, '/', 1, '='],
            expected: '2'
        },
        {
            array: [3, 0, '/', 2, '='],
            expected: '15'
        },
        {
            array: ['/', 5, '='],
            expected: '0'
        },
        {
            array: [1, 0, 0, '/', 2, 5, '='],
            expected: '4'
        },
    ];

    dataTest.forEach(data => {
        const {array, expected} = data;

        for (let i = 0; i < array.length; i++) {
            document.getElementsByName(array[i])[0].click();
        }

        const actual = document.getElementById('inputField').value;

        it(`Should return ${expected} when input data was ${array} `, () => {
            assert.strictEqual(actual, expected)
        });
        Clear();
    })
});